from celery import shared_task

def fake_input():
    return "new notification 42"

@shared_task
def filter_two(content):
    print(content)

@shared_task
def filter_one(content):
    if "42" in content:
        filter_two.delay(content)

@shared_task
def first_input():
    # parse json from notification api and fill the queue
    filter_one.delay(fake_input())
