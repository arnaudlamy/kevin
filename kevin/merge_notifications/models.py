from django.db import models

# Create your models here.

class Rule(models.Model):
    key = models.CharField(max_length=20)
    value = models.CharField(max_length=20)
