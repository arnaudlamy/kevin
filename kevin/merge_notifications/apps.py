from django.apps import AppConfig


class MergeNotificationsConfig(AppConfig):
    name = 'merge_notifications'
